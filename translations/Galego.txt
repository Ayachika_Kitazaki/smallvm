# MicroBlocks translation file
# Last updated: April 14 2020

# Blocks and categories

Output
Saída

set user LED _
acender LED integrado _

say _
dicir _

graph _
engadir á gráfica _

Input
Entrada

button A
botón A

button B
botón B

microseconds
microsegundos

milliseconds
milisegundos

board type
tipo de placa

Pins
Pins

read digital pin _
lectura dixital _

read analog pin _
lectura analóxica _

set digital pin _ to _
poñer o pin dixital _ a _

set pin _ to _
poñer o pin _ a _

analog pins
pins analóxicos

digital pins
pins dixitais

Control
Control

when started
ao comezar

when button _ pressed
cando se preme o botón _

forever _
por sempre _

repeat _ _
repetir _ veces _

wait _ millisecs
agardar _ milisegundos

if _ _
se _ _

else if _ _
se non, se _ _

if _ _ else _
se _ _ se non _

when _
cando _

wait until _
agardar ata que _

wait _ microsecs
agardar _ microsegundos

return _
retornar _

when _ received
ao recibir _

broadcast _
difundir _

comment _
comentario _

for _ in _ _
por cada _ en _ _

repeat until _ _
repetir ata que _ _

stop this task
deter esta tarefa

stop other tasks
--MISSING--

stop all
detelo todo

last message
--MISSING--

Operators
--MISSING--

_ mod _
_ módulo _

abs _
valor absoluto de _

random _ to _
número ao chou entre _ e _

not _
non _

_ and _
_ e _

and _
e _

_ or _
_ ou _

or _
ou _

_ is a _
--MISSING--

boolean
--MISSING--

number
--MISSING--

string
--MISSING--

list
--MISSING--

hex _
hexadecimal _

Variables
Variábeis

# Buttons on top of "Variables" category

Add a variable
Crear unha variábel

Delete a variable
Eliminar unha variábel

# New variable dialog

New variable name?
Novo nome da variábel?

set _ to _
asignar _ a _

change _ by _
aumentar _ en _

local _ _
variábel local _ _

Data
--MISSING--

list
--MISSING--

length of _
largo de _

item _ of _
elemento _ de _

replace item _ of list _ with _
--MISSING--

delete item _ of list _
--MISSING--

add _ to list _
--MISSING--

join _ _
--MISSING--

copy _ from _
--MISSING--

to _
--MISSING--

find _ in _
--MISSING--

starting at _
--MISSING--

join items of list _
--MISSING--

separator _
--MISSING--

unicode _ of _
--MISSING--

string from unicode _
--MISSING--

new list length _
nova lonxitude da lista _

new byte array length _
--MISSING--

free memory
--MISSING--

fill list _ with _
encher a lista _ con _

My Blocks
Os meus bloques

# Buttons on top of "My Blocks" category

Add a command block
Engadir un bloque de ordes

Add a reporter block
Engadir un bloque de reportes

# Make a block dialog

Enter function name:
Nome da función:

Comm
Comunicacións

i2c get device _ register _
--MISSING--

i2c set device _ register _ to _
i2c escribir no dispositivo _ o rexistro _ con _

i2c device _ read list _
--MISSING--

i2c device _ write list _
--MISSING--

spi send _
spi enviar _

spi receive
spi ler

print _
escribir _

no op
no op

ignore
ignorar

# Primitives (mostly hidden from end users)

draw shape _ at x _ y _
debuxar a forma _ en x _ y _

shape for letter _
forma para a letra _

set NeoPixel pin _ is RGBW _
poñer un NeoPixel no pin _ tipo RGBW _

send NeoPixel rgb _
enviar RGB _ ao NeoPixel

has tone support
admite tons

play tone pin _ frequency _
toca un ton no pin _ na frecuencia _

has WiFi support
admite WiFi

start WiFi _ password _
iniciar a WiFi _ co contrasinal _

stop WiFi
deter a WiFi

WiFi status
estado da WiFi

my IP address
o meu enderezo IP

radio send number _
enviar número _ por radio

radio send string _
enviar texto _ por radio

radio send pair _ = _
enviar asociación _ = _ por radio

radio message received?
mensaxe de radio recibida?

radio last number
último número recibido por radio

radio last string
último texto recibido por radio

radio last message type
tipo da última mensaxe recibido por radio

radio set group _
pon o grupo de radio a _

radio set channel (0-83) _
pon a canle de rádio a (0-83) _

radio set power (0-7) _
pon a potencia da radio a (0-7) _

radio last signal strength
intensidade da última señal de radio

radio receive packet _
recepción de paquetes por radio _

radio send packet _
enviar paquetes por radio _

disable radio
desactivar a radio

# Libraries

Basic Sensors
Sensores básicos

tilt x
inclinación x

tilt y
inclinación y

tilt z
inclinación z

acceleration
--MISSING--

light level
nivel de luz

temperature (°C)
temperatura (°C)

NeoPixel
--MISSING--

set NeoPixels _ _ _ _ _ _ _ _ _ _
poñer NeoPixeis _ _ _ _ _ _ _ _ _ _

clear NeoPixels
apagar NeoPixeis

set NeoPixel _ color _
poñer NeoPixel _ de cor _

set all NeoPixels color _
poñer todos os NeoPixeis de cor _

rotate NeoPixels by _
rotar NeoPixeis en _

color r _ g _ b _ (0-31)
--MISSING--

random color
--MISSING--

attach _ LED NeoPixel strip to pin _
--MISSING--

has white _
--MISSING--

PIR
--MISSING--

PIR at pin _ detected movement
--MISSING--

ED1 Stepper Motor
Motores ED1

move motor _ _ steps _
--MISSING--

move motor 1 _ and motor 2 _ _ steps
mover o motor 1 en _ e o motor 2 en _ _ pasos

move motor _ angle _ °
--MISSING--

move motor _ _ complete turns
--MISSING--

stop steppers
deter motores

clockwise
sentido horario

counter-clockwise
sentido antihorario

ED1 Buttons
Botóns ED1

button OK
botón Aceptar

button X
botón X

button up
botón arriba

button down
botón abaixo

button left
botón esquerda

button right
botón dereita

capacitive OK
capacitivo Aceptar

capacitive X
capacitivo X

capacitive up
capacitivo arriba

capacitive down
capacitivo abaixo

capacitive left
capacitivo esquerda

capacitive right
capacitivo dereita

capacitive sensor _
sensor capacitivo _

set capacitive threshold to _
poñer o limiar capacitivo a _

set use capacitive to _
utilizar botóns capacitivos _

Hummingbird LED _ _ %
Hummingbird LED _ _ %

Hummingbird Tri-LED _ R _ % G _ % B _ %
Hummingbird Tri-LED _ Vermello _ % Verde _ % Azul _ %

Hummingbird Position Servo _ _ °
--MISSING--

Hummingbird Rotation Servo _ _ %
Hummingbird Servo Rotacional _ _ %

Hummingbird _ _
Hummingbird _ _

Hummingbird Battery (mV)
Hummingbird Batería (mV)

Light
Luz

Distance (cm)
Distancia (cm)

Dial
Dial

Sound
Son

Other
Outros

distance (cm) trigger _ echo _
distancia (cm) disparador _ eco _

IR Remote
Infravermellos

receive IR code
recibir código IR dende

receive IR code from device _
recibir código IR dende o dispositivo _

test IR
probar IR

attach IR receiver to pin _
inicializar o receptor IR no pin _

Radio
Radio

Scrolling
Texto animado

scroll text _
animar o texto _

scroll number _
animar o número _

pausing _ ms
--MISSING--

stop scrolling
deter a animación

Servo
Servomotores

set servo _ to _ degrees (-90 to 90)
--MISSING--

set servo _ to speed _ (-100 to 100)
--MISSING--

stop servo _
--MISSING--

LED Display
Pantalla LED

display _
pantalla _

clear display
limpar pantalla

plot x _ y _
acender x _ y _

unplot x _ y _
apagar x _ y _

display character _
amosar carácter _

enable TFT _
activar TFT _

TFT width
--MISSING--

TFT height
--MISSING--

set TFT pixel x _ y _ to _
poñer o píxel x _ y _ da cor _

draw line on TFT from x _ y _ to x _ y _ color _
debuxar liña de x _ y _ a x _ y _ cor _

draw rectangle on TFT at x _ y _ width _ height _ color _
--MISSING--

draw rounded rectangle on TFT at x _ y _ width _ height _ radius _ color _
--MISSING--

draw circle on TFT at x _ y _ radius _ color _
--MISSING--

draw triangle on TFT at x _ y _ , x _ y _ , x _ y _ color _
--MISSING--

filled _
--MISSING--

write _ on TFT at x _ y _ color _
--MISSING--

scale _ wrap _
--MISSING--

Tone
Tons

attach buzzer to pin _
inicializar zunidor no pin _

play note _ octave _ for _ ms
tocar a nota _ na oitava _ durante _ ms

play frequency _ for _ ms
tocar a frecuencia _ durante _ ms

play midi key _ for _ ms
tocar a tecla MIDI _ durante _ ms

Turtle
--MISSING--

home
--MISSING--

move _
--MISSING--

turn _ degrees
--MISSING--

turn _ / _ of circle
--MISSING--

pen down
--MISSING--

pen up
--MISSING--

set pen color to _
--MISSING--

set pen to random color
--MISSING--

fill display with _
--MISSING--

go to x _ y _
--MISSING--

point in direction _
--MISSING--

WiFi
--MISSING--

wifi connect to _ password _ try _ times
conectar á WiFi _ co contrasinal _ con _ intentos

wifi create hotspot _ password _
crear punto de acceso WiFi _ co contrasinal _

IP address
enderezo IP

Motion
--MISSING--

motion
--MISSING--

start step counter
--MISSING--

step count
--MISSING--

clear step count
--MISSING--

set step threshold _ (0-50)
--MISSING--

Button Events
--MISSING--

button _ double pressed
--MISSING--

button _ long pressed
--MISSING--

button _ pressed
--MISSING--

Calliope set LED red _ green _ blue _
--MISSING--

Calliope set speaker _
--MISSING--

Calliope loudness
--MISSING--

Circuit Playground set speaker _
--MISSING--

Circuit Playground slide switch
--MISSING--

attach _ DotStar LEDs to data pin _ clock pin _
--MISSING--

set all DotStar LEDs to r _ g _ b _
--MISSING--

set DotStar LED _ to r _ g _ b _
--MISSING--

set DotStar brightness _
--MISSING--

bme280 connected
--MISSING--

bmp280 connected
--MISSING--

bmx280 temperature
--MISSING--

bmx280 pressure
--MISSING--

bme280 humidity
--MISSING--

TCS34725 connected
--MISSING--

TCS34725 rgb
--MISSING--

color _ name
--MISSING--

temperature (Celsius) DHT11 pin _
--MISSING--

humidity DHT11 pin _
--MISSING--

temperature (Celsius) DHT22 pin _
--MISSING--

humidity DHT22 pin _
--MISSING--

read PN532 RFID
--MISSING--

RFID _ = _
--MISSING--

get PN532 firmware version
--MISSING--

HTTP client
--MISSING--

_ data _ to http܃// _
--MISSING--

HTTP server
--MISSING--

start HTTP server
--MISSING--

HTTP server request
--MISSING--

respond _ to HTTP request
--MISSING--

with body _
--MISSING--

and headers _
--MISSING--

body of request _
--MISSING--

path of request _
--MISSING--

method of request _
--MISSING--

Web Thing
--MISSING--

set thing name to _
--MISSING--

set thing capability to _
--MISSING--

set boolean property _ title _ @Type _
--MISSING--

set string property _ title _ @Type _
--MISSING--

set number property _ title _ @Type _
--MISSING--

set number property _ title _ min _ max _ @Type _
--MISSING--

read only _
--MISSING--

register event _ type _
rexistrar o evento _ do tipo _

start WebThing server
--MISSING--

trigger event _
--MISSING--

thing description JSON
--MISSING--

properties JSON
--MISSING--

event definitions JSON
--MISSING--

events JSON
--MISSING--

# MicroBlocks UI buttons, error & info messages, dialog boxes, etc

New
Novo

Open
Abrir

Save
Gardar

Connect
Conectar

disconnect
desconectar

Serial port:
Porto serie:

other...
outro...

none
ningún

Port name?
Nome do porto?

Board type:
Tipo de placa:

Select board:
Seleccionar a placa:

Could not read:
Non foi posíbel ler:

by
por

Created with GP
--MISSING--

More info at http://microblocks.fun
Máis información en http://microblocks.fun

Function "
A función «

" is too large to send to board.
» é grande de máis para enviala á placa.

Script is too large to send to board.
O programa é grande de máis para envialo á placa..

Use "Connect" button to connect to a MicroBlocks device.
Utilice o botón «Conectar» para conectarse a un dispositivo MicroBlocks.

No boards found; is your board plugged in?
Non se atopou ningunha placa; ten conectada a súa placa?

For AdaFruit boards, double-click reset button and try again.
--MISSING--

The board is not responding.
A placa non responde.

Try to Install MicroBlocks on the board?
Tentar instalar MicroBlocks na placa?

The MicroBlocks in your board is not current
--MISSING--

Try to update MicroBlocks on the board?
--MISSING--

Stop
Deter

Start
Iniciar

Quit MicroBlocks?
Saír de MicroBlocks?

Discard current project?
Quere desbotar o proxecto actual?

clean up
limpeza

arrange scripts
organizar programas

undrop (ctrl-Z)
--MISSING--

copy all scripts to clipboard
copiar todos os programas no portappeis

paste all scripts
pegar todos os programas

paste script
pegar o programa

save a picture of all scripts
gardar a imaxe de todos os programas

about...
sobre...

virtual machine version
versión da máquina virtual

update firmware on board
actualizar o firmware da placa

show data graph
amosar a gráfica de datos

set serial delay
axustar a latencia do porto serie

firmware version
versión do firmware

start WebThing server
iniciar o servidor de WebThings

stop WebThing server
deter o servidor de WebThings

MicroBlocks HTTP Server listening on port 6473
o servidor HTTP de MicroBlocks esta activo no porto 6473

disable autoloading board libraries
--MISSING--

enable autoloading board libraries
--MISSING--

erase flash and update firmware on ESP board
--MISSING--

Use board type
--MISSING--

Wiping board...
--MISSING--

(press ESC to cancel)
--MISSING--

Done!
--MISSING--

download and install latest VM
--MISSING--

Select board type:
--MISSING--

Uploading MicroBlocks to board...
--MISSING--

copy data to clipboard
copiar os datos no portapapeis

clear data
limpar os datos

clear memory and variables
limpar a memoria e as variábeis

show advanced blocks
amosar os bloques avanzados

export functions as library
exportar as funcións como unha biblioteca

hide advanced blocks
agochar os bloques avanzados

Data Graph
Gráfico de datos

show instructions
amosar as instrucións

show compiled bytes
amosar os bytes compilados

expand
expandir

collapse
contraer

rename...
renomear...

show block definition...
amosar a definición do bloque...

show the definition of this block
amosa a definición deste bloque

delete block definition...
eliminar a definición do bloque...

delete the definition of this block
elimina a definición deste bloque

duplicate
duplicar

duplicate this block
duplicar este bloque

delete block
eliminar bloque

delete this block
eliminar este bloque

just this one block
só este bloque

copy to clipboard
copiar no portapapeis

copy these blocks to the clipboard
copia estes bloques no portapapeis

duplicate all
duplicar ata abaixo

duplicate these blocks
duplica este bloque e todos os que lle seguen

extract block
extraer bloque

pull out this block
sacar este bloque

save picture of script
gardar a imaxe do programa

save a picture of this block definition as a PNG file
gardar unha imaxe da definición deste bloque en formato PNG

save a picture of these blocks as a PNG file
garda unha imaxe destes bloques como un ficheiro PNG

copy script
copia el programa

delete
eliminar

Input type:
Tipo de entrada:

string only
só texto

string or number
texto ou número

number only
só número

define
definir

number/string
número/texto

editable number or string
número ou texto editábel

label
etiqueta

input
entrada

hide block definition
agochar a definición do bloque

Are you sure you want to remove this block definition?
Confirma que quere retirar a definición deste bloque?

Language
Idioma

Custom...
Personalizado...

Obsolete
Obsoleto

OK
Aceptar

Ok
Aceptar

Yes
Si

No
Non

Cancel
Cancelar

Okay
Aceptar

Confirm
Confirmar

# File picker and library dialogs

Libraries
Bibliotecas

Examples
--MISSING--

Desktop
--MISSING--

Computer
--MISSING--

Cloud
--MISSING--

File
Ficheiro

File Open
Abrir ficheiro

File Save
Gardar ficheiro

File name:
Nome de ficheiro:

New Folder
--MISSING--

by
por

Depends:
--MISSING--

Tags:
--MISSING--

Path, name or URL for library?
--MISSING--

Invalid URL
--MISSING--

Could not fetch library.
--MISSING--

Host does not exist or is currently down.
--MISSING--

File not found in server.
--MISSING--

Server expects HTTPS, and MicroBlocks doesn't currently support it.
--MISSING--

library information
--MISSING--

built-in library
--MISSING--

Dependency path, name or URL?
--MISSING--

Tag name?
--MISSING--

