module 'IR Remote' Comm
author MicroBlocks
version 1 1 
tags ir infrared remote 
description 'Read signals from infrared remotes, like the ones used for TV sets or air conditioners.
'
variables _ir_pin _ir_pulse_times 

	spec 'r' 'receiveIR' 'receive IR code'
	spec ' ' 'attachIR' 'attach IR receiver to pin _' 'num' 0
	spec ' ' '_testIR' 'test IR'
	spec 'r' '_receiveIRFromDevice' 'receive IR code from device _' 'num' -1
	spec ' ' '_captureIRMessage' '_captureIRMessage' 'any'
	spec ' ' '_dumpIR' '_dumpIR' 'any'
	spec 'r' '_getIRByte' '_getIRByte _' 'auto any' 4
	spec 'r' '_got32Bits' '_got32Bits' 'any'

to '_captureIRMessage' {
  if (_ir_pulse_times == 0) {
    _ir_pulse_times = (newList 200)
    if (and (_ir_pin == 0) ((boardType) == 'CircuitPlayground')) {
      _ir_pin = 11
    } (and (_ir_pin == 0) ((boardType) == 'Citilab ED1')) {
      _ir_pin = 35
    } (and (_ir_pin == 0) ((boardType) == 'D1-Mini')) {
      _ir_pin = 2
    }
  }
  fillList _ir_pulse_times 0
  local 'i' 1
  comment 'Wait for IR signal -- this is the start of a new message.
Note: THe pin goes low when an IR signal is detected.'
  waitUntil (not (digitalReadOp _ir_pin))
  local 'start' (microsOp)
  forever {
    comment 'Record the time until the end of the current IR pulse ("mark")'
    waitUntil (digitalReadOp _ir_pin)
    local 'end' (microsOp)
    atPut i _ir_pulse_times (end - start)
    i += 1
    start = end
    comment 'Record time until the start of the next IR pulse ("space")'
    repeatUntil (not (digitalReadOp _ir_pin)) {
      if (((microsOp) - start) > 5000) {
        comment 'No IR pulse for 5000 usecs means "end of message"'
        return 0
      }
    }
    local 'end' (microsOp)
    atPut i _ir_pulse_times (end - start)
    i += 1
    start = end
  }
}

to '_dumpIR' {
  comment 'Print raw pulse timings to the terminal.
Can be used to analyze new protocols.'
  local 'i' 1
  printIt '-----'
  repeat (size _ir_pulse_times) {
    local 'mark usecs' (at i _ir_pulse_times)
    local 'space usecs' (at (i + 1) _ir_pulse_times)
    printIt (v 'mark usecs') (v 'space usecs')
    i += 2
    if ((v 'space usecs') == 0) {
      printIt 'timing entries:' (i - 2)
      return 0
    }
  }
}

to '_getIRByte' position {
  local 'result' 0
  local 'i' position
  repeat 8 {
    if ((at i _ir_pulse_times) < 1000) {
      result = ((result << 1) | 0)
    } else {
      result = ((result << 1) | 1)
    }
    i += 2
  }
  return result
}

to '_got32Bits' {
  return (and ((at 67 _ir_pulse_times) != 0) ((at 68 _ir_pulse_times) == 0))
}

to '_receiveIRFromDevice' deviceID {
  forever {
    '_captureIRMessage'
    if ('_got32Bits') {
      local 'id' ('_getIRByte' 20 nil)
      if (id == deviceID) {
        return ('_getIRByte' 36 nil)
      }
    }
  }
}

to '_testIR' {
  forever {
    '_captureIRMessage'
    if ('_got32Bits') {
      local 'b1' ('_getIRByte' 4 nil)
      local 'b2' ('_getIRByte' 20 nil)
      local 'b3' ('_getIRByte' 36 nil)
      local 'b4' ('_getIRByte' 52 nil)
      sayIt 'Device:' b2 'code:' b3
    }
  }
}

to attachIR pin {
  _ir_pin = pin
}

to receiveIR {
  forever {
    '_captureIRMessage'
    if ('_got32Bits') {
      return ('_getIRByte' 36 nil)
    }
  }
}

