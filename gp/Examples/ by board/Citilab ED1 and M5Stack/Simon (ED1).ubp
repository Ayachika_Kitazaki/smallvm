module main
author unknown
version 1 0 
description ''
variables _loop_overhead 'current index' sequence 

	spec ' ' 'initialize letters' 'initialize sequence'
	spec 'r' 'correct guess' 'correct guess at _' 'auto' '10'
	spec 'r' 'wrong guess' 'wrong guess at _' 'auto' '10'
	spec ' ' 'play note at' 'play note at _' 'auto' '10'
	spec ' ' 'triangle up' 'draw  _' 'auto' 1

to 'correct guess' index {
  return (or (or (and ('up button') ((at index sequence) == 1)) (and ('down button') ((at index sequence) == 2))) (or (and ('left button') ((at index sequence) == 3)) (and ('right button') ((at index sequence) == 4))))
}

to 'initialize letters' {
  'play tone' 'C' 1 100
  'play tone' 'E' 1 100
  'play tone' 'G' 1 100
  'play tone' 'C' 2 100
  if (sequence == 0) {
    sequence = (newList 20)
  }
  for i (size sequence) {
    atPut i sequence (random 1 4)
  }
}

to 'play note at' index {
  'triangle up' index
  if ((at index sequence) == 1) {
    'play tone' 'E' 1 200
  } ((at index sequence) == 2) {
    'play tone' 'F#' 1 200
  } ((at index sequence) == 3) {
    'play tone' 'D' 1 200
  } else {
    'play tone' 'G' 1 200
  }
}

to 'triangle up' index {
  '[display:mbDisplayOff]'
  if ((at index sequence) == 1) {
    '[tft:triangle]' 0 0 63 63 126 0 (hexToInt '00FF00') true
  } ((at index sequence) == 2) {
    '[tft:triangle]' 0 126 63 63 126 126 (hexToInt 'FF0000') true
  } ((at index sequence) == 3) {
    '[tft:triangle]' 0 0 63 63 0 126 (hexToInt '0000FF') true
  } else {
    '[tft:triangle]' 126 0 63 63 126 126 (hexToInt '00FFFF') true
  }
}

to 'wrong guess' index {
  return (or (or (and ('up button') ((at index sequence) != 1)) (and ('down button') ((at index sequence) != 2))) (or (and ('left button') ((at index sequence) != 3)) (and ('right button') ((at index sequence) != 4))))
}

script 52 50 {
comment 'This is the Simon memory game for the
Citilab ED1 board'
}

script 413 50 {
whenBroadcastReceived 'player turn'
local 'index' 1
repeatUntil (index > (v 'current index')) {
  if ('correct guess' index) {
    'play note at' index
    index += 1
    waitUntil (and (and (not ('up button')) (not ('down button'))) (and (not ('left button')) (not ('right button'))))
  } ('wrong guess' index) {
    '[display:mbDisplayOff]'
    '[tft:text]' 'Sorry' 30 45 (colorSwatch 189 191 3 255) 2 true
    '[tft:text]' 'Try again...' 25 70 (colorSwatch 184 184 184 255) 1 true
    'play tone' 'G' 1 250
    'play tone' 'F#' 1 250
    'play tone' 'F' 1 250
    'play tone' 'E' 1 250
    waitMillis 500
    sendBroadcast 'go!'
    stopTask
  }
}
waitMillis 500
'current index' += 1
sendBroadcast 'play sequence'
}

script 50 100 {
whenStarted
sendBroadcast 'go!'
}

script 202 105 {
whenBroadcastReceived 'go!'
'current index' = 1
'initialize letters'
waitMillis 1500
sendBroadcast 'play sequence'
}

script 51 233 {
whenBroadcastReceived 'play sequence'
if ((v 'current index') > 10) {
  '[display:mbDisplayOff]'
  '[tft:text]' 'You win!!' 15 45 (colorSwatch 189 8 191 255) 2 true
  repeat 2 {
    'play tone' 'C' 2 100
    'play tone' 'E' 1 100
  }
  'play tone' 'C' 1 100
  'play tone' 'G' 1 100
  'play tone' 'C' 2 200
  waitMillis 500
  sendBroadcast 'go!'
} else {
  for i (v 'current index') {
    'play note at' i
  }
  sendBroadcast 'player turn'
}
}


module 'ED1 Buttons' Input
author MicroBlocks
version 1 0 
tags button capacitive ed1 
description 'Provides blocks for the six capacitive buttons in the Citilab ED1 board.'
variables _ED1_buttons_init '_capacitive threshold' 

	spec ' ' '_ED1_buttons_init' '_ED1_buttons_init' 'any any any'
	spec 'r' 'OK button' 'button OK'
	spec 'r' 'cancel button' 'button X'
	spec 'r' 'up button' 'button up'
	spec 'r' 'down button' 'button down'
	spec 'r' 'left button' 'button left'
	spec 'r' 'right button' 'button right'
	spec ' ' 'set capacitive threshold to' 'set capacitive threshold to _' 'auto' 16

to 'OK button' {
  '_ED1_buttons_init'
  return (('[sensors:touchRead]' 15) < (v '_capacitive threshold'))
}

to '_ED1_buttons_init' {
  if (_ED1_buttons_init == 0) {
    if ((v '_capacitive threshold') == 0) {'_capacitive threshold' = 16}
    _ED1_buttons_init = (booleanConstant true)
  }
}

to 'cancel button' {
  '_ED1_buttons_init'
  return (('[sensors:touchRead]' 14) < (v '_capacitive threshold'))
}

to 'down button' {
  '_ED1_buttons_init'
  return (('[sensors:touchRead]' 13) < (v '_capacitive threshold'))
}

to 'left button' {
  '_ED1_buttons_init'
  return (('[sensors:touchRead]' 2) < (v '_capacitive threshold'))
}

to 'right button' {
  '_ED1_buttons_init'
  return (('[sensors:touchRead]' 27) < (v '_capacitive threshold'))
}

to 'set capacitive threshold to' threshold {
  '_capacitive threshold' = threshold
}

to 'up button' {
  '_ED1_buttons_init'
  return (('[sensors:touchRead]' 4) < (v '_capacitive threshold'))
}


module TFT Output
author MicroBlocks
version 1 1 
tags tft graphics draw 
description 'Draw graphics and write text on boards with a TFT display, such as the M5Stack, M5Stick, Citilab ED1 or (discontinued) IoT-Bus.'

	spec ' ' '[display:mbDisplayOff]' 'clear display'
	spec ' ' '[tft:rect]' 'draw rectangle on TFT at x _ y _ width _ height _ color _ : filled _' 'num num num num color bool' 10 10 40 30 nil true
	spec ' ' '[tft:roundedRect]' 'draw rounded rectangle on TFT at x _ y _ width _ height _ radius _ color _ : filled _' 'num num num num num color bool' 10 10 40 30 8 nil true
	spec ' ' '[tft:circle]' 'draw circle on TFT at x _ y _ radius _ color _ : filled _' 'num num num color bool' 40 40 30 nil true
	spec ' ' '[tft:triangle]' 'draw triangle on TFT at x _ y _ , x _ y _ , x _ y _ color _ : filled _' 'num num num num num num color bool' 20 20 30 80 60 5 nil true
	spec ' ' '[tft:text]' 'write _ on TFT at x _ y _ color _ : scale _ wrap _' 'str num num color num bool' 'Hello World!' 5 5 nil 2 true
	spec ' ' '[tft:line]' 'draw line on TFT from x _ y _ to x _ y _ color _' 'num num num num color' 12 8 25 15
	spec ' ' '[tft:setPixel]' 'set TFT pixel x _ y _ to _' 'auto auto color' '10' '10'
	spec 'r' 'makeColor' 'color r _ g _ b _ (0-255)' 'auto auto auto' 0 100 100
	spec 'r' 'randomColor' 'random color'
	spec 'r' '[tft:getWidth]' 'TFT width'
	spec 'r' '[tft:getHeight]' 'TFT height'
	spec ' ' '[tft:enableDisplay]' 'enable TFT _' 'bool' true

to makeColor r g b {
  r = (maximum 0 (minimum r 255))
  g = (maximum 0 (minimum g 255))
  b = (maximum 0 (minimum b 255))
  return ((r << 16) | ((g << 8) | b))
}

to randomColor {
  local 'n1' (random 100 200)
  local 'n2' (random 0 100)
  if (1 == (random 1 3)) {
    return ((n1 << 16) | (n2 << 8))
  } (1 == (random 1 2)) {
    return ((n2 << 16) | n1)
  } else {
    return ((n1 << 8) | n2)
  }
}


module Tone Output
author MicroBlocks
version 1 2 
tags tone sound music audio note speaker 
description 'Audio tone generation. Make music with MicroBlocks!
'
variables _tonePin _toneInitalized _toneLoopOverhead 

	spec ' ' 'play tone' 'play note _ octave _ for _ ms' 'auto num num' 'C' 0 500
	spec ' ' 'playMIDIKey' 'play midi key _ for _ ms' 'num num' 60 500
	spec ' ' 'play frequency' 'play frequency _ for _ ms' 'num num' 261 500
	spec ' ' 'attach buzzer to pin' 'attach buzzer to pin _' 'auto' ''
	spec 'r' '_measureLoopOverhead' '_measureLoopOverhead'
	spec 'r' '_baseFreqForNote' '_baseFreqForNote _' 'auto' 'c'
	spec 'r' '_baseFreqForSharpOrFlat' '_baseFreqForSharpOrFlat _' 'auto' 'c#'
	spec 'r' '_baseFreqForSemitone' '_baseFreqForSemitone _' 'num' 0
	spec ' ' '_toneLoop' '_toneLoop _ for _ ms' 'num num' 440000 100

to '_baseFreqForNote' note {
  comment 'Return the frequency for the given note in the middle-C octave
scaled by 1000. For example, return 440000 (440Hz) for A.
Note names may be upper or lower case. Note names
may be followed by # for a sharp or b for a flat.'
  if (or (note == 'c') (note == 'C')) {
    return 261626
  } (or (note == 'd') (note == 'D')) {
    return 293665
  } (or (note == 'e') (note == 'E')) {
    return 329628
  } (or (note == 'f') (note == 'F')) {
    return 349228
  } (or (note == 'g') (note == 'G')) {
    return 391995
  } (or (note == 'a') (note == 'A')) {
    return 440000
  } (or (note == 'b') (note == 'B')) {
    return 493883
  }
  return ('_baseFreqForSharpOrFlat' note)
}

to '_baseFreqForSemitone' semitone {
  if (0 == semitone) {
    return 261626
  } (1 == semitone) {
    return 277183
  } (2 == semitone) {
    return 293665
  } (3 == semitone) {
    return 311127
  } (4 == semitone) {
    return 329628
  } (5 == semitone) {
    return 349228
  } (6 == semitone) {
    return 369994
  } (7 == semitone) {
    return 391995
  } (8 == semitone) {
    return 415305
  } (9 == semitone) {
    return 440000
  } (10 == semitone) {
    return 466164
  } (11 == semitone) {
    return 493883
  }
}

to '_baseFreqForSharpOrFlat' note {
  comment 'Return the frequency for the given sharp or flat note in the
middle-C octave scaled by 1000. Only handles black keys.
Thus, you can''t write E# to mean F.'
  if (or (or (note == 'c#') (note == 'C#')) (or (note == 'db') (note == 'Db'))) {
    return 277183
  } (or (or (note == 'd#') (note == 'D#')) (or (note == 'eb') (note == 'Eb'))) {
    return 311127
  } (or (or (note == 'f#') (note == 'F#')) (or (note == 'gb') (note == 'Gb'))) {
    return 369994
  } (or (or (note == 'g#') (note == 'G#')) (or (note == 'ab') (note == 'Ab'))) {
    return 415305
  } (or (or (note == 'a#') (note == 'A#')) (or (note == 'bb') (note == 'Bb'))) {
    return 466164
  }
  comment 'Unrecognized note names map to 0.1 Hz, which is inaudible.
This helps users find typos in their tunes.'
  return 100
}

to '_measureLoopOverhead' {
  comment 'Measure the loop overhead on this device'
  local 'halfCycle' 100
  local 'startT' (microsOp)
  repeat 100 {
    digitalWriteOp _tonePin false
    waitMicros halfCycle
    digitalWriteOp _tonePin false
    waitMicros halfCycle
  }
  local 'usecs' ((microsOp) - startT)
  return ((usecs - 20000) / 200)
}

to '_toneLoop' scaledFreq ms {
  if (_toneInitalized == 0) {'attach buzzer to pin' ''}
  if ('[io:hasTone]') {
    '[io:playTone]' _tonePin (scaledFreq / 1000)
    waitMillis ms
    '[io:playTone]' _tonePin 0
  } else {
    local 'halfCycle' ((500000000 / scaledFreq) - _toneLoopOverhead)
    local 'cycles' ((ms * 500) / halfCycle)
    repeat cycles {
      digitalWriteOp _tonePin true
      waitMicros halfCycle
      digitalWriteOp _tonePin false
      waitMicros halfCycle
    }
  }
}

to 'attach buzzer to pin' pinNumber {
  if (pinNumber == '') {
    comment 'Pin number not specified; use default pin for this device'
    if ((boardType) == 'Citilab ED1') {
      _tonePin = 26
    } ((boardType) == 'M5Stack-Core') {
      _tonePin = 25
    } ((boardType) == 'M5StickC') {
      _tonePin = 26
    } ((boardType) == 'Calliope') {
      digitalWriteOp 23 true
      digitalWriteOp 24 true
      _tonePin = 25
    } ((boardType) == 'D1-Mini') {
      _tonePin = 12
    } else {
      _tonePin = -1
    }
  } else {
    _tonePin = pinNumber
  }
  _toneLoopOverhead = ('_measureLoopOverhead')
  _toneInitalized = (booleanConstant true)
}

to 'play frequency' freq ms {
  '_toneLoop' (freq * 1000) ms
}

to 'play tone' note octave ms {
  local 'freq' ('_baseFreqForNote' note)
  if (octave < 0) {
    repeat (absoluteValue octave) {
      freq = (freq / 2)
    }
  }
  repeat octave {
    freq = (freq * 2)
  }
  '_toneLoop' freq ms
}

to playMIDIKey key ms {
  local 'freq' ('_baseFreqForSemitone' (key % 12))
  local 'octave' ((key / 12) - 5)
  if (octave < 0) {
    repeat (absoluteValue octave) {
      freq = (freq / 2)
    }
  }
  repeat octave {
    freq = (freq * 2)
  }
  '_toneLoop' freq ms
}

