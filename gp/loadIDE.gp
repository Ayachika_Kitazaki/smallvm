reload '../ide/MicroBlocksAppMaker.gp'
reload '../ide/MicroBlocksCompiler.gp'
reload '../ide/MicroBlocksDataGraph.gp'
reload '../ide/MicroBlocksDecompiler.gp'
reload '../ide/MicroBlocksEditor.gp'
reload '../ide/MicroBlocksFlasher.gp'
reload '../ide/MicroBlocksLibraryWidgets.gp'
reload '../ide/MicroBlocksProject.gp'
reload '../ide/MicroBlocksRuntime.gp'
reload '../ide/MicroBlocksScripter.gp'
reload '../ide/MicroBlocksThingServer.gp'
reload '../ide/MicroBitDisplaySlot.gp'
reload '../ide/MicroBlocksPatches.gp'
reload '../ide/ESPTool.gp'

to startup {
	// Open the MicroBlocks IDE in developer mode
	openMicroBlocksEditor true
}
